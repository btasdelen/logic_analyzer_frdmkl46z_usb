/*
 * SerialComm.c
 *
 *  Created on: 8 Eyl 2016
 *      Author: bilal
 */

#include "SerialComm.h"

comm_mode_t changeMode() {
	if ((1 == s_cdcVcom.attach) && (1 == s_cdcVcom.startTransactions))
		{
			/* User Code */
			if ((0 != s_recvSize) && (0xFFFFFFFFU != s_recvSize))
			{
				s_recvSize = 0;
				switch (s_currRecvBuf[CMD_BUFF_POS]) {
					case START_CMD:
						return CONFIG_MODE;

					case START_SAMP:
						return SAMPLING_MODE;

					default:
						return ERR_MODE;
				}
			}
		}
	return IDLE;
}

comm_config_state_t changeConfigMode() {

	switch (s_currRecvBuf[CMD_MODE_BUFF_POS]) {
		case GET_MAX_CH_NUM:
		case GET_MAX_SAMP_RATE:
		case GET_AVAIL_MEM_SIZE:
			return REPLY_CMD;

		case SET_SAMP_RATE:
		case SET_SAMP_DURATION:
			return SET_CONFIG;

		default:
			return ERR_UNKNOWN_CONFIG;
	}

}

config_status_t replyCmd() {
	uint32_t size = 4;
	switch (s_currRecvBuf[CMD_MODE_BUFF_POS]) {
		case GET_MAX_SAMP_RATE:
			s_currSendBuf[1] = (uint8_t)(MAX_SAMPLING_FREQ >> 16);
			s_currSendBuf[2] = (uint8_t)(MAX_SAMPLING_FREQ >> 8);
			s_currSendBuf[3] = (uint8_t)(MAX_SAMPLING_FREQ);
			break;

		case GET_MAX_CH_NUM:
			s_currSendBuf[1] = 0;
			s_currSendBuf[2] = 0;
			s_currSendBuf[3] = MAX_CH_NUM;
			break;

		case GET_AVAIL_MEM_SIZE:
			s_currSendBuf[1] = (uint8_t)(AVAIL_MEM_SIZE >> 16);
			s_currSendBuf[2] = (uint8_t)(AVAIL_MEM_SIZE >> 8);
			s_currSendBuf[3] = (uint8_t)(AVAIL_MEM_SIZE);
			break;
		default:
			return UNKNOWN_CMD;
	}
	s_recvSize = 0;
	s_currSendBuf[0] = SUCCESS;
	usb_status_t usb_err = USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT, s_currSendBuf, size);
	if (usb_err != kStatus_USB_Success) return TRANSFER_ERR;
	else return SUCCESS;
}

config_status_t applyCmd() {
	uint32_t freq;
	switch (s_currRecvBuf[CMD_MODE_BUFF_POS]) {
		case SET_SAMP_RATE:
			freq = (((uint32_t)s_currRecvBuf[CMD_MODE_BUFF_POS + 1U]) << 16)
							| (((uint32_t)s_currRecvBuf[CMD_MODE_BUFF_POS + 2U]) << 8)
							| ((uint32_t)s_currRecvBuf[CMD_MODE_BUFF_POS + 3U]);

			if (!setSamplingFrequency(freq)) {return INVALID_SAMP_FREQ;}
			break;

		case SET_SAMP_DURATION:
			samplingDuration = (((uint32_t)s_currRecvBuf[CMD_MODE_BUFF_POS + 1U]) << 16)
							| (((uint32_t)s_currRecvBuf[CMD_MODE_BUFF_POS + 2U]) << 8)
							| ((uint32_t)s_currRecvBuf[CMD_MODE_BUFF_POS + 3U]);
			break;
		default:
			return UNKNOWN_CMD;
	}
	s_currSendBuf[0] = SUCCESS;
	usb_status_t usb_err = USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT, s_currSendBuf, 1);
	if (usb_err != kStatus_USB_Success) return TRANSFER_ERR;
	else return SUCCESS;
}


