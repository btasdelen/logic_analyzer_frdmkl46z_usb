#include "Sampling.h"

dma_handle_t dma_handle;
dma_transfer_config_t transferCfgBuffer1;
dma_transfer_config_t transferCfgBuffer2;
volatile uint16_t c_rep = 1;
volatile bool currBuff = true;

void DMA_OneShot_Callback(dma_handle_t *handle, void *param)
{
	DMA_AbortTransfer(handle);
	PIT_StopTimer(PIT, 0);
    USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT, dataBuffer, samplingDuration);
	isSampling = false;
}

void DMA_MultiBuffering_Callback(dma_handle_t *handle, void *param)
{
	DMA_AbortTransfer(handle);
	if(!(--c_rep)) {
		isSampling = false;
		PIT_StopTimer(PIT, 0);
		if(currBuff) {
			USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT, dataBuffer, (AVAIL_MEM_SIZE >> 1));
		}
		else {
			USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT,
					(uint8_t*)(&dataBuffer[AVAIL_MEM_SIZE >> 1]), (AVAIL_MEM_SIZE >> 1));
		}
	}
	else {
		if(currBuff) {
			DMA_SubmitTransfer(&dma_handle, &transferCfgBuffer2, kDMA_EnableInterrupt);
			DMA_StartTransfer(handle);
			currBuff = false;
			USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT, dataBuffer, (AVAIL_MEM_SIZE >> 1));
		}
		else {
			DMA_SubmitTransfer(&dma_handle, &transferCfgBuffer1, kDMA_EnableInterrupt);
			DMA_StartTransfer(handle);
			currBuff = true;
			USB_DeviceSendRequest(s_cdcVcom.deviceHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT,
					(uint8_t*)(&dataBuffer[AVAIL_MEM_SIZE >> 1]), (AVAIL_MEM_SIZE >> 1));
		}
	}

}

void initChannels(void) {
	gpio_pin_config_t ch_conf = {kGPIO_DigitalInput, 0};

	for (int ch = 0; ch < 8; ++ch) {
		GPIO_PinInit(CH_GPIO, ch, &ch_conf);
	}

	pit_config_t pit_cfg;
	PIT_GetDefaultConfig(&pit_cfg);
	pit_cfg.enableRunInDebug = true;

	PIT_Init(PIT, &pit_cfg);
	PIT_SetTimerPeriod(PIT, 0, 1000);


    /* Initialize and enable DMA. */
    DMAMUX_Init(DMAMUX0);
    DMAMUX_EnablePeriodTrigger(DMAMUX0, 0);
    DMAMUX_SetSource(DMAMUX0, 0, kDmaRequestMux0AlwaysOn60);
    DMAMUX_EnableChannel(DMAMUX0, 0);
    dma_transfer_config_t transferConfig;

    DMA_Init(DMA0);
    DMA_CreateHandle(&dma_handle, DMA0, 0);
    DMA_SetCallback(&dma_handle, DMA_OneShot_Callback, NULL);
    DMA_PrepareTransfer(&transferConfig, (void *)((uint32_t)(&CH_GPIO->PDIR)), 1U,
    					(void *)(&dataBuffer[0]), 1U, AVAIL_MEM_SIZE, kDMA_PeripheralToMemory);

    DMA_SubmitTransfer(&dma_handle, &transferConfig, kDMA_EnableInterrupt);

    /* Enable IRQ. */
    NVIC_EnableIRQ(DMA0_IRQn);

    // Default sampling frequency is 1 MHz.
    setSamplingFrequency(1000000);

}

void startSampling(void) {
    isSampling = true;
	DMA_AbortTransfer(&dma_handle);
    if (samplingDuration > AVAIL_MEM_SIZE) {
		DMA_SetCallback(&dma_handle, DMA_MultiBuffering_Callback, NULL);

		DMA_PrepareTransfer(&transferCfgBuffer1, (void *)((uint32_t)(&CH_GPIO->PDIR)), 1U,
		    					(void *)(&dataBuffer[0]), 1U, (AVAIL_MEM_SIZE >> 1), kDMA_PeripheralToMemory);

		DMA_PrepareTransfer(&transferCfgBuffer2, (void *)((uint32_t)(&CH_GPIO->PDIR)), 1U,
		    					(void *)(&dataBuffer[AVAIL_MEM_SIZE >> 1]), 1U, (AVAIL_MEM_SIZE >> 1), kDMA_PeripheralToMemory);

		DMA_SubmitTransfer(&dma_handle, &transferCfgBuffer1, kDMA_EnableInterrupt);

		c_rep = ceil(((float)samplingDuration)/((float)(AVAIL_MEM_SIZE >> 1)));
	}
    else {
        dma_transfer_config_t transferConfig;

		DMA_SetCallback(&dma_handle, DMA_OneShot_Callback, NULL);
		DMA_PrepareTransfer(&transferConfig, (void *)((uint32_t)(&CH_GPIO->PDIR)), 1U,
		    					(void *)(&dataBuffer[0]), 1U, samplingDuration, kDMA_PeripheralToMemory);

		DMA_SubmitTransfer(&dma_handle, &transferConfig, kDMA_EnableInterrupt);
	}

    DMA_StartTransfer(&dma_handle);
    PIT_StartTimer(PIT, 0);
}

bool setSamplingFrequency(uint32_t freq) {
	uint32_t bus_freq = CLOCK_GetBusClkFreq();

	if(freq >= bus_freq) {
		return false;
	}
	else {
		PIT_StopTimer(PIT, 0);
		uint16_t modv = (uint16_t)round((((double)bus_freq)/((double)(freq))) - 1);
		PIT_SetTimerPeriod(PIT, 0, modv);
	}
	samplingFreq = freq;
	return true;
}

void setSamplingDurationUS(uint32_t us) {
	float periodUS = 1.0/((float)samplingFreq);
	samplingDuration = ((uint32_t)ceil(((float)us)/periodUS));
}

