#include "board.h"
#include "Sampling.h"
#include "virtual_com.h"

#define CMD_BUFF_POS 0U
#define CMD_MODE_BUFF_POS 1U
#define ERR_MSG_SIZE 2U

enum _comm_mode{IDLE, CONFIG_MODE, SAMPLING_MODE , ERR_MODE}
		typedef comm_mode_t;

enum _comm_config_state{REPLY_CMD = 1, SET_CONFIG, ERR_UNKNOWN_CONFIG}
		typedef comm_config_state_t;

enum _config_status{SUCCESS = 0x0, INVALID_SAMP_FREQ = 0x1, TRANSFER_ERR = 0x2 ,UNKNOWN_CMD = 0xFF }
		typedef config_status_t;


enum _serial_cmd{   START_CMD = 0x1,
					GET_MAX_CH_NUM = 0x2,
					GET_MAX_SAMP_RATE = 0x3,
					GET_AVAIL_MEM_SIZE = 0x4,
					SET_SAMP_RATE = 0xA,
					SET_SAMP_DURATION = 0xB,
					SEND_ERR = 0x7E,
					START_SAMP = 0xFE,
					END_SAMP = 0xFF} typedef serial_cmd_t;


comm_mode_t changeMode();
comm_config_state_t changeConfigMode();
config_status_t replyCmd();
config_status_t applyCmd();
